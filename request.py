# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields, ModelView, ModelSQL, Exclude
from trytond.pyson import Or, Not, Equal, Eval, Bool, If
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.modules.stock_unit_load import cases_digits
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond import backend
from itertools import groupby
from sql.conditionals import Coalesce, Case, Greatest
from sql.operators import Equal as SqlEqual
from sql.aggregate import Sum, Count
from sql import Null, With


class Request(metaclass=PoolMeta):
    __name__ = 'agro.harvest.request'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({
            'assign_order': {
                'icon': 'tryton-add',
                'invisible': Not(Equal(Eval('state'), 'draft')),
                'depends': ['state']
            }
        })

    @classmethod
    def confirm(cls, records):
        pool = Pool()
        RequestLine = pool.get('agro.harvest.request.line')
        Order = pool.get('agro.production.order')

        lines = [l for r in records for l in r.lines if r.state != 'close']
        RequestLine.check_production_order_ul_quantity(lines)

        super().confirm(records)

        orders = [line.production_order for line in lines
            if line.production_order]
        if orders:
            Order.recompute_unit_loads_to_produce(list(set(orders)))

    @classmethod
    @ModelView.button_action(
        'agro_sale_board_harvest_request.wizard_assign_order')
    def assign_order(cls, records):
        pass

    @property
    def allowed_crops(self):
        Batch = Pool().get('agro.farm.batch')
        domain = []
        if self.year:
            domain = [('year', '=', self.year.id)]
        if self.farm:
            domain.append(('farm', '=', self.farm.id))
        if self.producer:
            domain.append(('producer', '=', self.producer.id))
        batches = Batch.search(domain)
        return list(set(b.crop.id for b in batches if b.crop)) or []


class RequestLine(metaclass=PoolMeta):
    __name__ = 'agro.harvest.request.line'

    party = fields.Many2One('party.party', 'Party',
        states={
            'readonly': (
                (Eval('request_state') != 'draft')
                | Eval('close')
                | Eval('lines', [])
                | Eval('production_order'))
        },
        depends=['request_state', 'close', 'production_order', 'lines'])
    production_order = fields.Many2One('agro.production.order',
        'Production order',
        domain=[
            ('process', '!=', None),
            If(Bool(Eval('party')),
                ['OR',
                    [
                        ('shipment_party', '=', Eval('party'))],
                    [
                        ('shipment_party', '=', None),
                        ('party', '=', Eval('party'))
                    ]
                ],
                []),
            ('crop', 'in', Eval('allowed_crops'))
        ],
        states={
            'readonly': Or(
                Not(Equal(Eval('request_state'), 'draft')),
                Bool(Eval('close')))
        },
        depends=['party', 'request_state', 'close', 'allowed_crops'])
    allowed_crops = fields.Function(
        fields.Many2Many('agro.crop', None, None, 'Allowed crops'),
        'on_change_with_allowed_crops')

    @classmethod
    def _get_domain_order_states(cls):
        return ['confirmed']

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('request_line_order_exclude', Exclude(t,
                (t.request, SqlEqual),
                (Coalesce(t.production_order, -1), SqlEqual),
                where=(t.production_order != Null)),
            'agro_sale_board_harvest_request.'
            'msg_agro_harvest_request_line_request_line_order_exclude')]

        cls.production_order.domain = [If(Eval('request_state') == 'draft',
            cls.production_order.domain + [
                ('state', 'in', cls._get_domain_order_states()),
                ('load_state', '=', 'none')], []
        )]

        for fname in ('ul_cases_quantity', 'quantity_per_case', 'quantity'):
            _field = getattr(cls, fname)
            _field.states['readonly'] |= Bool(Eval('production_order'))
            _field.depends.append('production_order')

    @classmethod
    def __register__(cls, module_name):
        pool = Pool()
        Order = pool.get('agro.production.order')

        TableHandler = backend.TableHandler
        table = TableHandler(cls, module_name)
        sql_table = cls.__table__()
        order = Order.__table__()
        cursor = Transaction().connection.cursor()

        party_exist = table.column_exist('party')
        super().__register__(module_name)
        if not party_exist:
            cursor.execute(*sql_table.update(
                [sql_table.party],
                order.select(
                    Coalesce(order.shipment_party, order.party),
                    where=order.id == sql_table.production_order)))

    @fields.depends('production_order', '_parent_production_order.category',
        '_parent_production_order.uom', '_parent_production_order.crop_group',
        '_parent_production_order.crop',
        '_parent_production_order.ul_quantity',
        '_parent_production_order.ul_cases_quantity',
        '_parent_production_order.quantity', '_parent_production_order.lines',
        methods=[
            'get_cases_quantity',
            'get_quantity_per_case',
            'get_quantity_per_ul',
            'copy_attributes',
            '_get_product_from_production_order'
        ])
    def on_change_production_order(self):
        if self.production_order:
            order = self.production_order
            self.party = order.shipment_party or order.party
            self.category = order.category
            self.uom = order.uom
            self.crop_group = order.crop_group
            self.crop = order.crop
            self.copy_attributes(order)
            self.ul_quantity = order.ul_quantity
            self.ul_cases_quantity = order.ul_cases_quantity
            self.quantity = order.quantity
            self.cases_quantity = self.get_cases_quantity()
            self.quantity_per_case = self.get_quantity_per_case()
            self.quantity_per_ul = self.get_quantity_per_ul()
            self._get_product_from_production_order()

    def _get_product_from_production_order(self):
        pool = Pool()
        Template = pool.get('product.template')

        order = self.production_order
        templates = Template.search([
            ('base_product.crops', '=', order.crop.id) if order.crop else
            ('base_product.crop_groups', '=', order.crop_group),
            ('category_attribute', '=', order.attribute_category),
            ('caliber_attribute', '=', order.attribute_caliber),
            ('confection_attribute', '=', order.attribute_confection)])
        if len(templates) == 1:
            self.product = templates[0].products[0]
            self.base_product = templates[0].base_product

    @fields.depends('request', '_parent_request.year', '_parent_request.farm',
        '_parent_request.producer')
    def on_change_with_allowed_crops(self, name=None):
        if self.request:
            return list(map(int, self.request.allowed_crops))
        return []

    @classmethod
    def check_production_order_ul_quantity(cls, records):
        recs = [r for r in records if r.production_order]
        recs.sort(key=lambda x: x.production_order)
        for order, rq_lines in groupby(recs, key=lambda x: x.production_order):
            rq_lines = list(rq_lines)
            rq_lines_ids = list(map(int, rq_lines))

            lines = cls.search([
                ('request.state', 'in', ['confirmed', 'close']),
                ('production_order', '=', order.id),
                ('id', 'not in', rq_lines_ids)])

            all_rq_lines_ids = list(map(int, lines)) + rq_lines_ids
            uls = [ul for ul in order.sale_unit_loads
                if ul.harvest_line and ul.harvest_line.request_line
                and ul.harvest_line.request_line.id not in all_rq_lines_ids
            ]

            req_ul_quantity = sum(r.ul_quantity for r in rq_lines)
            req_ul_quantity += sum(r.ul_quantity or 0 for r in lines)
            req_ul_quantity += len(uls)
            if req_ul_quantity > order.ul_quantity_to_request:
                raise UserError(gettext(
                    'agro_sale_board_harvest_request.'
                    'msg_agro_harvest_request_line_too_many_uls_order',
                    order=order.rec_name,
                    req_ul_quantity=req_ul_quantity,
                    ul_quantity=order.ul_quantity))

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('production_order', None)
        return super().copy(records, default=default)

    @property
    def bom_used(self):
        if self.production_order:
            return self.production_order.bom
        return super().bom_used

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Order = pool.get('agro.production.order')

        lines = super().create(vlist)

        orders = [line.production_order for line in lines
            if line.production_order]
        if orders:
            Order.recompute_unit_loads_to_produce(list(set(orders)))
        return lines

    @classmethod
    def write(cls, *args):
        pool = Pool()
        Order = pool.get('agro.production.order')

        super().write(*args)

        orders = [line.production_order
            for grouped_lines in args[::2] for line in grouped_lines
            if line.production_order]

        if orders:
            Order.recompute_unit_loads_to_produce(list(set(orders)))


class AssignOrderStart(ModelView):
    """Assign Order Start"""
    __name__ = 'agro.harvest.request.assign_order.start'

    request = fields.Many2One('agro.harvest.request', 'Request', readonly=True)
    production_orders = fields.One2Many(
        'agro.harvest.request.assign_order.data', None,
        'Production orders',
        # enable add/remove because m2m does not let editing in tree view
        add_remove=[
            ('production_order.company', '=', Eval('_parent_request.company')),
            ['OR',
                ('request_line', '=', None),
                ('request_line.request', '!=', Eval('request'))
            ],
            ('id', 'not in', Eval('production_orders'))
        ], depends=['request', 'production_orders'])


class AssignOrderData(ModelView, ModelSQL):
    """Assign Order Data"""
    __name__ = 'agro.harvest.request.assign_order.data'
    _rec_name = 'code'

    request_line = fields.Many2One('agro.harvest.request.line', 'Request line',
        readonly=True)
    production_order = fields.Many2One('agro.production.order',
        'Production order', readonly=True)
    code = fields.Char('Code', readonly=True)
    departure_date = fields.Date('Departure date', readonly=True)
    arrival_date = fields.Date('Arrival date', readonly=True)
    reference = fields.Char('Reference', readonly=True)
    party = fields.Many2One('party.party', 'Party', readonly=True)
    shipment_party = fields.Many2One('party.party', 'Shipment party',
        readonly=True)
    shipment_address = fields.Many2One('party.address', 'Shipment address',
        readonly=True)
    crop_group = fields.Many2One('agro.crop.group', 'Crop group',
        readonly=True)
    crop = fields.Many2One('agro.crop', 'Crop', readonly=True)
    attribute_category = fields.Many2One('agro.product.attribute.value',
        'Category', readonly=True)
    attribute_caliber = fields.Many2One('agro.product.attribute.value',
        'Caliber', readonly=True)
    attribute_confection = fields.Many2One('agro.product.attribute.value',
        'Confection', readonly=True)
    ul_quantity = fields.Float('ULs', digits=(16, 0), readonly=True)
    ul_cases_quantity = fields.Float('Cases per UL', readonly=True,
        digits=cases_digits)
    quantity_per_ul = fields.Function(
        fields.Float('Quantity per UL',
            digits=(16, Eval('uom_digits', 0)),
            depends=['uom_digits']),
        'get_quantity_per_ul')
    quantity_per_case = fields.Function(
        fields.Float('Quantity per Case',
            digits=(16, Eval('uom_digits', 0)),
            depends=['uom_digits']),
        'get_quantity_per_case')
    quantity = fields.Float('Quantity', readonly=True,
        digits=(16, Eval('uom_digits', 0)),
        depends=['uom_digits'])
    cases_quantity = fields.Float('Cases', readonly=True,
        digits=cases_digits)
    uom = fields.Many2One('product.uom', 'UOM', readonly=True)
    uom_digits = fields.Function(
        fields.Integer('UOM digits'),
        'get_uom_digits')
    ul_quantity_to_request = fields.Float('UL quantity to request',
        digits=(16, 0), domain=[
            ('ul_quantity_to_request', '<=', Eval('ul_quantity_to_assign'))
        ],
        depends=['ul_quantity_to_assign'])
    ul_quantity_requested = fields.Float('UL quantity requested',
        digits=(16, 0), readonly=True)
    ul_quantity_to_assign = fields.Float('UL quantity to assign',
        digits=(16, 0), readonly=True)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._order.insert(0, ('departure_date', 'DESC'))
        cls._order.insert(1, ('ul_quantity_to_assign', 'DESC'))

    @classmethod
    def table_query(cls):
        pool = Pool()
        Order = pool.get('agro.production.order')
        Request = pool.get('agro.harvest.request')
        RequestLine = pool.get('agro.harvest.request.line')
        StockUnitLoad = pool.get('stock.unit_load')

        order = Order.__table__()
        request_line = RequestLine.__table__()
        stock_unit_load = StockUnitLoad.__table__()

        request = None

        where = ((order.process != Null))

        if Transaction().context.get('active_id'):
            request = Request(Transaction().context['active_id'])
            where &= (order.crop.in_(request.allowed_crops))
            if request.state == 'draft':
                where &= (
                    order.state.in_(RequestLine._get_domain_order_states())
                    & (order.load_state == 'none'))

        subquery_qty_requested = order.join(
            request_line,
            condition=order.id == request_line.production_order
        ).select(
            order.id.as_('id'),
            Sum(request_line.ul_quantity).as_('ul_quantity'),
            group_by=order.id
        )

        # Get sale_ul_quantity
        subquery_qty_sale = With()
        subquery_qty_sale.query = stock_unit_load.join(
                order,
                condition=(order.id == stock_unit_load.sale_production_order)
            ).select(
                order.id.as_('id'),
                Count(stock_unit_load.id).as_('ul_quantity'),
                group_by=[order.id]
            )

        return order.join(request_line, 'LEFT', condition=(
                (order.id == request_line.production_order)
                & (request_line.request
                    == Transaction().context.get('active_id', 0)))
            ).join(
                subquery_qty_requested, 'LEFT', condition=(
                    order.id == subquery_qty_requested.id)
            ).join(
                subquery_qty_sale, 'LEFT', condition=(
                    subquery_qty_sale.id == order.id)
            ).select(
                order.id.as_('id'),
                order.id.as_('production_order'),
                request_line.id.as_('request_line'),
                order.code,
                order.reference,
                order.departure_date,
                order.arrival_date,
                order.party,
                order.shipment_party,
                order.shipment_address,
                order.crop_group,
                order.crop,
                order.attribute_category,
                order.attribute_caliber,
                order.attribute_confection,
                order.ul_quantity,
                order.ul_cases_quantity,
                (order.ul_quantity * order.ul_cases_quantity).as_(
                    'cases_quantity'),
                order.quantity,
                order.uom,
                Case((request_line.id != Null,
                    Greatest(request_line.ul_quantity
                        - Coalesce(subquery_qty_sale.ul_quantity, 0), 0)),
                    else_=Greatest(order.ul_quantity
                        - Coalesce(subquery_qty_requested.ul_quantity, 0)
                        - Coalesce(subquery_qty_sale.ul_quantity, 0), 0)
                    ).as_('ul_quantity_to_request'),
                Coalesce(subquery_qty_requested.ul_quantity, 0).as_(
                    'ul_quantity_requested'),
                Case((request_line.id != Null,
                    Greatest(request_line.ul_quantity
                        - Coalesce(subquery_qty_sale.ul_quantity, 0), 0)),
                    else_=Greatest(order.ul_quantity
                        - Coalesce(subquery_qty_requested.ul_quantity, 0)
                        - Coalesce(subquery_qty_sale.ul_quantity, 0), 0)
                    ).as_('ul_quantity_to_assign'),
                order.create_date,
                order.write_date,
                order.create_uid,
                order.write_uid,
                where=where,
                with_=[subquery_qty_sale]
            )

    def get_uom_digits(self, name=None):
        if not self.uom:
            return 2
        return self.uom.digits

    def get_quantity_per_ul(self, name=None):
        if self.ul_quantity and self.quantity and self.uom:
            return self.uom.round(self.quantity / self.ul_quantity)
        elif self.ul_cases_quantity and self.quantity_per_case:
            return self.ul_cases_quantity * self.quantity_per_case
        return None

    def get_quantity_per_case(self, name=None):
        pool = Pool()
        Uom = pool.get('product.uom')

        if self.quantity and self.cases_quantity and self.uom:
            return self.uom.round(self.quantity / self.cases_quantity)

        qty_per_case, qty_per_case_uom = (
            self.production_order._get_qty_per_case())
        if qty_per_case:
            if qty_per_case_uom:
                qty_per_case = Uom.compute_qty(
                    qty_per_case_uom, qty_per_case, self.uom)
            return qty_per_case
        return None

    @classmethod
    def view_attributes(cls):
        return super().view_attributes() + [
            ('/tree', 'visual',
                If((Eval('ul_quantity_to_assign', 0) <= 0),
                    'muted',
                    '')
            )
        ]


class AssignOrder(Wizard):
    """Assign Order"""
    __name__ = 'agro.harvest.request.assign_order'

    start = StateView('agro.harvest.request.assign_order.start',
        'agro_sale_board_harvest_request.request_assign_order_start_view_form',
        [Button('Cancel', 'end', 'trytond-cancel'),
         Button('OK', 'add_', 'tryton-ok', default=True)])

    add_ = StateTransition()

    def default_start(self, fields):
        pool = Pool()
        OrderData = pool.get('agro.harvest.request.assign_order.data')

        order_data = OrderData.search([
            ('request_line.request', '=', self.record)
        ])

        return {
            'request': self.record.id,
            'production_orders': list(map(int, order_data))
        }

    def transition_add_(self):
        pool = Pool()
        Request = pool.get('agro.harvest.request')
        RequestLine = pool.get('agro.harvest.request.line')

        to_save = []
        request = Request(Transaction().context['active_id'])
        for order in self.start.production_orders:
            if not order.ul_quantity_to_request:
                raise UserError(gettext(
                    'agro_sale_board_harvest_request.'
                    'msg_agro_harvest_request_assign_order_missing_ul_quantity',
                    order=order.rec_name))
            if order.request_line:
                request_line = order.request_line
                if request_line.ul_quantity != order.ul_quantity_to_request:
                    request_line.ul_quantity = order.ul_quantity_to_request
                    request_line.on_change_ul_quantity()
            else:
                request_line = RequestLine()
                request_line.request = request
                request_line.production_order = order.production_order
                request_line.on_change_production_order()
                request_line.ul_quantity = order.ul_quantity_to_request
                request_line.on_change_ul_quantity()
            to_save.append(request_line)

        if to_save:
            RequestLine.save(to_save)
        return 'end'


class SaleBoardProduct(metaclass=PoolMeta):
    __name__ = 'agro.harvest.request.line'

    @fields.depends('_parent_production_order.base_product',
        '_parent_production_order.product')
    def _get_product_from_production_order(self):
        if self.production_order.product:
            self.base_product = self.production_order.base_product
            self.product = self.production_order.product
        else:
            super()._get_product_from_production_order()


class LineBOM(metaclass=PoolMeta):
    __name__ = 'agro.harvest.request.line'

    @fields.depends('bom', 'production_order', methods=['copy_bom'])
    def on_change_production_order(self):
        super().on_change_production_order()
        if self.production_order:
            self.copy_bom(self.production_order.bom)


class LineExtraBOM(metaclass=PoolMeta):
    __name__ = 'agro.harvest.request.line'

    def on_change_production_order(self):
        super().on_change_production_order()
        if self.production_order:
            self.extra_bom = self.production_order.extra_bom
