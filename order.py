# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.i18n import gettext
from trytond.exceptions import UserError


class ProductionOrder(metaclass=PoolMeta):
    __name__ = 'agro.production.order'

    harvest_request_lines = fields.One2Many('agro.harvest.request.line',
        'production_order', 'Harvest request lines', readonly=True)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._deny_modify_requested = {
            'crop',
            'attribute_category',
            'attribute_caliber',
            'attribute_confection'
        }

    @classmethod
    def recompute_unit_loads_to_produce(cls, records):
        pool = Pool()
        RequestLine = pool.get('agro.harvest.request.line')
        OrderLine = pool.get('agro.production.order.line')

        to_save = []
        for record in records:
            rlines = RequestLine.search([
                ('production_order', '=', record),
                ('request_state', 'in', ['confirmed', 'done'])])

            remaining_uls = record.ul_quantity or 0
            if rlines:
                remaining_uls -= sum(l.ul_quantity or 0 for l in rlines)

            uls2reduce = sum(
                l.ul_quantity or 0 for l in record.lines) - remaining_uls
            if uls2reduce <= 0:
                continue
            lines = sorted(record.lines, key=lambda l: l.ul_created_quantity)
            for line in lines:
                if not uls2reduce:
                    break
                if line.ul_created_quantity >= line.ul_quantity:
                    continue
                diff = min(line.ul_quantity - line.ul_created_quantity,
                    uls2reduce)
                line.ul_quantity -= diff
                uls2reduce -= diff
                to_save.append(line)

        if to_save:
            OrderLine.save(to_save)

    @property
    def ul_quantity_to_request(self):
        if self.ul_quantity is None:
            return
        return max(self.ul_quantity - (self.sale_ul_quantity or 0), 0)

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('harvest_request_lines', None)
        return super().copy(records, default=default)

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        for records, values in zip(actions, actions):
            _deny_fields = cls._deny_modify_requested & set(values)
            if _deny_fields:
                requested = [r for r in records if r.harvest_request_lines]
                if requested:
                    _fields = cls.fields_get(_deny_fields)
                    raise UserError(gettext('agro_sale_board_harvest_request.'
                        'msg_modify_order_with_harvest_request_lines',
                        order=requested[0].rec_name,
                        deny_fields='\n  - '.join(
                            v['string'] for v in _fields.values())))
        return super().write(*args)


class ProductionOrderProduct(metaclass=PoolMeta):
    __name__ = 'agro.production.order'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._deny_modify_requested |= {'base_product', 'product'}


class ProductionOrderAutoLine(metaclass=PoolMeta):
    __name__ = 'agro.production.order'

    @classmethod
    def create_order_line(cls, records):
        super().create_order_line(records)
        cls.recompute_unit_loads_to_produce(records)
